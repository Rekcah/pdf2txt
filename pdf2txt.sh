#!/bin/bash

#Pacchetti prerequisiti:
#poppler
#tesseract
#tesseract-data-ita

read -p 'Inserisci il nome del pdf (es. "Walden\ 2.pdf") > ' pdf_file
nome_pdf=$(echo $pdf_file | sed 's/\.pdf//' | sed 's/ //g')

#Crea cartella delle immagini
echo "Creo la cartella $nome_pdf"
cartella_immagini="immagini_$nome_pdf"
mkdir $cartella_immagini

#Crea cartella dei txt
cartella_txt=$cartella_immagini/txt_$(echo $pdf_file | sed 's/\.pdf//' | sed 's/ //g')
mkdir $cartella_txt

#Converte le pagine del file pdf in png
echo "Sto convertendo il file pdf in file png, l'operazione potrà richiedere qualche minuto..."
pdftoppm -png "$(echo $pdf_file)" "$cartella_immagini/$nome_pdf"

#Converte i file png in txt
echo "Sto convertendo i file png in txt..."
for file_png in $cartella_immagini/*.png
do
	nome_txt=${file_png##*/}
	tesseract -l ita "$file_png" "$cartella_txt/$(echo $nome_txt | sed 's/\.png/\.txt/g')"
done

#Verifica sugli errori
if [ $? -ne 0 ]
then
	echo "Errore $?"
else
	echo "Conversione da pdf a txt completata, troverai i tuoi file sotto $cartella_pdf/$cartella_txt"
fi
